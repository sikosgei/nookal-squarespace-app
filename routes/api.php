<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

$clientController = App\Http\Controllers\ClientController::class;
Route::controller($clientController)->group(function () use ($clientController) {
    Route::get('fetch-client-from-nookal', [$clientController, 'fetchClientFromNookal']);
    Route::post('publish-client-to-squarespace', [$clientController, 'publishClientToSquareSpace']);
    Route::delete('delete-client-from-squarespace', [$clientController, 'deleteClientFromSquareSpace']);
    Route::get('list-square-space-clients', [$clientController, 'listSquareSpaceClients']);
    Route::get('get-appointments-nookal', [$clientController, 'getAppointmentFromNookal']);
    Route::post('publish-appointments-squarespace', [$clientController, 'publishAppointmentsToSquareSpace']);
    Route::get('get-appointments', [$clientController, 'getAppointmentsFromSquareSpace']);
});
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');
