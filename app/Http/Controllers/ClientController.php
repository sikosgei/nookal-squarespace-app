<?php
namespace App\Http\Controllers;
use App\Jobs\processBookingToSquareSpace;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Jobs\proccessClientToSquareSpace;
use App\Jobs\processClientFromNookal;
use App\Jobs\InsertAppoitmentsFromNoookal;
use App\Jobs\publishAppoitments;
use App\Jobs\deleteClientFromSquareSpace;

class ClientController extends Controller
{
    public function fetchClientFromNookal() {
        processClientFromNookal::dispatch();
    }
    public function publishClientToSquareSpace() {
        proccessClientToSquareSpace::dispatch();

    }
    public function listSquareSpaceClients() {
        $acuity = new \AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID') ?? 32600460,
            'apiKey' => env('ACUITY_API_KEY') ?? '1047594525f65d4c41e638600c9ea83c'
        ));
       $clients =  $acuity->request('/clients');
       return response()->json($clients);
    }
    public function getAppointmentsFromSquareSpace() {
        $acuity = new \AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID') ?? 32600460,
            'apiKey' => env('ACUITY_API_KEY') ?? '1047594525f65d4c41e638600c9ea83c'
        ));
       $clients =  $acuity->request('/appointments');
       return response()->json($clients);
    }
    public function getAppointmentFromNookal() {
        InsertAppoitmentsFromNoookal::dispatch();
    }
    public function publishAppointmentsToSquareSpace() {
        publishAppoitments::dispatch();
    }
    public function deleteClientFromSquareSpace()
    {
        deleteClientFromSquareSpace::dispatch();

    }
}
