<?php
namespace App\Services;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
class NookalServices {
    public static function makeApiRequest($method, $endpoint, $body = [], $queryParams = [])
    {
        $client = new Client([
            'base_uri' => 'https://api.nookal.com/production/v2/',
        ]);
        $headers = [
            'Accept' => 'application/json',
        ];
        $queryParams['api_key'] = env('NOOKAL_API') ?? 'Bdb2878a-c5b4-2B0b-DEA4-6B04b4d83D7C';
        $options = [
            'headers' => $headers,
            'query' => $queryParams,
        ];
        if ($method === 'POST') {
            $options['form_params'] = $body;
        }
        try {
            $response = $client->request($method, $endpoint, $options);
            return json_decode($response->getBody()->getContents(), true);
        } catch (RequestException $e) {
            return $e->getMessage();
        }
    }

}
