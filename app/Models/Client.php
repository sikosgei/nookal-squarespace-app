<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'firstName',
        'lastName',
        'phone',
        'email',
        'notes',
        'uploaded_to_square_space',
        'client_id'
    ];
    public function appoitments()
    {
        return $this->hasMany(Appointment::class,'patientID','client_id');
    }
}
