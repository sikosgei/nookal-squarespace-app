<?php

namespace App\Jobs;

use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;

class deleteClientFromSquareSpace implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 3600; // 1 hour
    protected $batchSize = 100;
    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $totalClients = Client::count();
        $batches = ceil($totalClients / $this->batchSize);
        for ($i = 0; $i < $batches; $i++) {
            $clients = Client::skip($i * $this->batchSize)->take($this->batchSize)->get();
            Bus::dispatch(new ProcessDelete($clients));
        }
    }
}
