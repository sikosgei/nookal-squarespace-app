<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Client;
use Exception;
use Illuminate\Support\Facades\Log;

class ProcessClientBatchToSquareSpace implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    protected $clients;

    public $timeout = 3600; // 1 hour

    /**
     * Create a new job instance.
     */
    public function __construct($clients)
    {
        $this->clients = $clients;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $acuity = new \AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID') ?? 32600460,
            'apiKey' => env('ACUITY_API_KEY') ?? '1047594525f65d4c41e638600c9ea83c'
        ));

        foreach ($this->clients as $client) {
            try {
                $clientData = [
                    'data' => [
                        'firstName' => $client->firstName,
                        'lastName' => $client->lastName,
                        'email' => $client->email,
                        'phone' => $client->phone,
                        'notes' => $client->notes,
                    ],
                    'method' => 'POST'
                ];
                $acuity->request('/clients', $clientData);} catch (Exception $e) {
                Log::error('Error populating client to Squarespace: ' . $e->getMessage());
            }
        }
    }
}
