<?php

namespace App\Jobs;

use App\Models\Appointment;
use App\Models\Client;
use App\Services\NookalServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class insertAppoitmentsFromNoookal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

         $data = NookalServices::makeApiRequest('GET', 'getAppointments');
        foreach ($data['data']['results']['appointments'] as $appointment) {
            $client = Client::where('client_id', $appointment['patientID'])->first();
            if ($client) {
                $appointmentData = [
                    'patientID' => $appointment['patientID'],
                    'datetime' => $appointment['appointmentStartDateTimeUTC'],
                    'appointmentTypeID' => $appointment['appointmentTypeID'],
                    'firstName' => $client->firstName,
                    'lastName' => $client->lastName,
                    'email' => $client->email,
                    'Notes' => $appointment['Notes'],
                    'fields' => json_encode([
                        'locationID' => $appointment['locationID'],
                        'practitionerID' => $appointment['practitionerID'],
                        'status' => $appointment['status'],
                    ]),
                ];
                Appointment::updateOrCreate(
                    [
                        'patientID' => $appointment['patientID'],
                        'datetime' => $appointment['appointmentStartDateTimeUTC']
                    ],
                    $appointmentData
                );
            }
        }
    }
}
