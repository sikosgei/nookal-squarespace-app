<?php

namespace App\Jobs;

use App\Models\Client;
use App\Services\NookalServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProcessClientFromNookal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 3600; // 1 hour

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // $data = NookalServices::makeApiRequest('GET', 'getPatients');
        //$patients = $data['data']['results']['patients'];
        //  foreach ($patients as $patient) {
        // Client::updateOrCreate(
        // ['email' => $patient['Email']],
        //  [
        //'firstName' => $patient['FirstName'],
        //    'lastName' => $patient['LastName'],
        //  'phone' => $patient['Mobile'],
        //     'email' => $patient['Email'],
        //'notes' => '',
           //     ]
           // );
      //  }
        $this->processClientsFromCsv();
    }

    /**
     * Process clients from CSV file.
     */
    private function processClientsFromCsv(): void
    {
        $filePath = storage_path('app/public/clients_original.csv');
        if (!file_exists($filePath)) {
            return;
        }
        $file = fopen($filePath, 'r');
        fgetcsv($file);
        while (($data = fgetcsv($file)) !== FALSE) {
            list($client_id, $firstName, $lastName, $mobile, $email) = $data;
            if (empty($firstName) || empty($lastName)) {
                continue;
            }
            $client = Client::updateOrCreate(
                [
                    'client_id' => $client_id,
                ],
                [
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'phone' => $mobile,
                    'email' => $email,
                    'notes' => '',
                ]
            );
            $client->update(['uploaded_to_square_space' => true]);
        }
        fclose($file);
    }
}

