<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    protected $clients;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     */
    public function __construct($clients)
    {
        $this->clients = $clients;
    }


    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $acuity = new \AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID') ?? 32600460,
            'apiKey' => env('ACUITY_API_KEY') ?? '1047594525f65d4c41e638600c9ea83c'
        ));

        foreach ($this->clients as $client) {
            try {
                $clientData = [
                    'data' => [
                        'firstName' => $client->firstName,
                        'lastName' => $client->lastName,
                        'phone' => $client->phone,
                    ],
                    'method' => 'DELETE'
                ];
                $acuity->request('/clients', $clientData);
             echo ($client->phone);
             echo "\n";
            } catch (\Exception $e) {
                Log::error('Error deleting client to Squarespace: ' . $e->getMessage());
            }
        }
    }
}
