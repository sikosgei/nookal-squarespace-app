<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class publishAppoitments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $acuity = new \AcuityScheduling(array(
            'userId' => env('ACUITY_USER_ID') ?? 32600460,
            'apiKey' => env('ACUITY_API_KEY') ?? '1047594525f65d4c41e638600c9ea83c'
        ));
        //$account_type = $acuity->request('/appointment-types');
        //dd($account_type);
        $allAppointments = \App\Models\Appointment::all();
        foreach ($allAppointments as $appointment) {
            $datetime = new \DateTime($appointment->datetime);
            $formattedDatetime = $datetime->format('c');
            $appointmentData = [
                'data' => [
                    'datetime' => "2024-06-24T11:00:00+0100",
                    'appointmentTypeID' => 64444520,
                    'firstName' => $appointment->firstName,
                    'lastName' => $appointment->lastName,
                    'email' => $appointment->email,
                    'notes' => $appointment->notes,
                    'phone' => +254720872130,
                    'fields' => json_decode($appointment->fields),
                ],
                'method' => 'POST'
            ];
            $appoitment = $acuity->request('/appointments', $appointmentData);
            dd($appoitment);
        }
        dd($acuity->request('/appointments'));
    }
}
